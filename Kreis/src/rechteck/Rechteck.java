package rechteck;

import java.awt.Point;

public class Rechteck {

	private Point startpunkt;
	private double a;
	private double b;
	
	public Rechteck(double radius, Point mittelpunkt)
	{
	  setStartpunkt(startpunkt);
	  setA(a);
	  setB(b);
	}
	
	//Getter&Setter fuer A
	public double getA() {
		return this.a;
	}	
	public void setA(double a) {
		 this.a = a;
	}
	//Getter&Setter fuer B
	public double getB() {
		return this.b;
	}	
	public void setB(double b) {
		 this.b = b;
	}	

	public double getFlaeche() {
		return (this.a*2)+(this.b*2);
	}

	public double getUmfang() {
		return (this.a+this.a)+(this.b+this.b);
	}
	
	public double getDiagonale() {
		return Math.sqrt(Math.pow(this.a, 2) + Math.pow(this.b, 2));
	}

	public void setStartpunkt(Point mittelpunkt) {
		this.startpunkt = mittelpunkt; 
	}
	
	public Point getStartpunkt() {
		return startpunkt;
	}
}//.