/**
 * 
 * @author DChojnacki
 *
 */
public class Ladung {
	/**
	 * Variablen werden deklariert.
	 */
	private String bezeichnung;
	private int menge;
/**
 * Vollparameterisierter Konstruktor Ladung
 * @param bezeichnung
 * @param menge
 */
	public Ladung(String bezeichnung, int menge) {
		setBezeichnung(bezeichnung);
		setMenge(menge);	
	}
/**
 * 
 * @return 
 */
	public String getBezeichnung() {
		return bezeichnung;
	}
/**
 * 
 * @param bezeichnung
 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
/**
 * 
 * @return
 */
	public int getMenge() {
		return this.menge;
	}
/**
 * 
 * @param menge
 */
	public void setMenge(int menge) {
		this.menge = menge;
	}

}