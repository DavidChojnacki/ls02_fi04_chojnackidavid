/**
 * 
 * @author DChojnacki
 *
 */
public class RaumschiffTest {
/**
 * Main Methode
 */
	public static void main(String[] args) {
		/**
		 * Raumschiff Objekte werden erstellt.
		 */
		Raumschiff klingonen = new Raumschiff(2, 100, 100, 100, 100, "IKS Hegh'ta", 2);
		Raumschiff romulaner = new Raumschiff(0, 50, 100, 100, 100, "IRW Khazara", 2);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni'Var", 5);
		/**
		 * Ladungs Objekte werden erstellt.
		 */
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		Ladung l3 = new Ladung("Rote Materie", 2);
		Ladung l4 = new Ladung("Forschungssonde", 35);
		Ladung l5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung l6 = new Ladung("Plasma-Waffe", 50);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		/**
		 * Ladung werden den Raumschiffen hinzugefügt.
		 */
		klingonen.addLadung(l1);
		klingonen.addLadung(l5);
		romulaner.addLadung(l2);
		romulaner.addLadung(l3);
		romulaner.addLadung(l6);
		vulkanier.addLadung(l4);
		vulkanier.addLadung(l7);
        /**
         * vorgegebene Aufgaben werden ausgeführt.
         * Methoden werden mittels erstellten Objekten aus der Raumschiffklasse aufgerufen.
         */
		klingonen.torpedoAbschiessen(romulaner);
		romulaner.phaserkanoneAbschiessen(klingonen);
		vulkanier.addBroadcastKommunikator(vulkanier.getSchiffsName() + ": Gewalt ist nicht logisch");
		vulkanier.ausgabeBroadcastKommunikator();
		klingonen.ausgabeLog();
		klingonen.ausgabeLadungsverzeichnis();
		klingonen.torpedoAbschiessen(romulaner);
		klingonen.torpedoAbschiessen(romulaner);
		klingonen.ausgabeLog();
		klingonen.ausgabeLadungsverzeichnis();
		romulaner.ausgabeLog();
		romulaner.ausgabeLadungsverzeichnis();
		vulkanier.ausgabeLog();
		vulkanier.ausgabeLadungsverzeichnis();

		//ignorieren (fuer Zukunftsprojekt)
		// // MENU
		// String[] menuEintraghaupt = { "Raumschiff-Info", "Photonentorpedos
		// abschießen", "Phaserkanonen abschießen" };
		// int menuLaenge = menuEintraghaupt.length;
		// System.out.printf("%-16s%-40s%n%n", "Nummer", "Bezeichnung");
		// for (int x = 0; x < menuLaenge; x++) {
		// System.out.printf("%-16d%-40s%n", (x + 1), menuEintraghaupt[x]);
		// }
		// System.out.print("\nWelche Option moechten Sie ausfuehren?: ");
		// int auswahl = tastatur.nextInt();
		// if (auswahl == 1) {
		// klingonen.ausgabeRaumschiffinfo();
		// romulaner.ausgabeRaumschiffinfo();
		// vulkanier.ausgabeRaumschiffinfo();
		// } else if (auswahl == 2) {
		// System.out.println("Welches Schiff schiesst? (klingonen, romulaner,
		// vulkanier");
		// String schiffauswahl = tastatur.next();
		// System.out.println("Welches Schiff wird beschossen? (klingonen, romulaner,
		// vulkanier");
		// String schifftreffen = tastatur.next();
		// if (schiffauswahl == "klingonen") {
		// if (schifftreffen == "klingonen") {
		// klingonen.torpedoAbschiessen(klingonen);
		// } else if (schifftreffen == "romulaner") {
		// klingonen.torpedoAbschiessen(romulaner);
		// } else if (schifftreffen == "vulkanier") {
		// klingonen.torpedoAbschiessen(vulkanier);
		// }
		// } else if (schiffauswahl == "romulaner") {
		// if (schifftreffen == "klingonen") {
		// romulaner.torpedoAbschiessen(klingonen);
		// } else if (schifftreffen == "romulaner") {
		// romulaner.torpedoAbschiessen(romulaner);
		// } else if (schifftreffen == "vulkanier") {
		// romulaner.torpedoAbschiessen(vulkanier);
		// }
		// } else if (schiffauswahl == "vulkanier") {
		// if (schifftreffen == "klingonen") {
		// vulkanier.torpedoAbschiessen(klingonen);
		// } else if (schifftreffen == "romulaner") {
		// vulkanier.torpedoAbschiessen(romulaner);
		// } else if (schifftreffen == "vulkanier") {
		// vulkanier.torpedoAbschiessen(vulkanier);
		// }
		//
		// }
		// } else if (auswahl == 3) {
		// System.out.println("Welches Schiff schiesst? (klingonen, romulaner,
		// vulkanier");
		// String schiffauswahl = tastatur.next();
		// System.out.println("Welches Schiff wird beschossen? (klingonen, romulaner,
		// vulkanier");
		// String schifftreffen = tastatur.next();
		// if (schiffauswahl == "klingonen") {
		// if (schifftreffen == "klingonen") {
		// klingonen.phaserkanoneAbschiessen(klingonen);
		// } else if (schifftreffen == "romulaner") {
		// klingonen.phaserkanoneAbschiessen(romulaner);
		// } else if (schifftreffen == "vulkanier") {
		// klingonen.phaserkanoneAbschiessen(vulkanier);
		// }
		// } else if (schiffauswahl == "romulaner") {
		// if (schifftreffen == "klingonen") {
		// romulaner.phaserkanoneAbschiessen(klingonen);
		// } else if (schifftreffen == "romulaner") {
		// romulaner.phaserkanoneAbschiessen(romulaner);
		// } else if (schifftreffen == "vulkanier") {
		// romulaner.phaserkanoneAbschiessen(vulkanier);
		// }
		// } else if (schiffauswahl == "vulkanier") {
		// if (schifftreffen == "klingonen") {
		// vulkanier.phaserkanoneAbschiessen(klingonen);
		// } else if (schifftreffen == "romulaner") {
		// vulkanier.phaserkanoneAbschiessen(romulaner);
		// } else if (schifftreffen == "vulkanier") {
		// vulkanier.phaserkanoneAbschiessen(vulkanier);
		// }
		// }
		// }
		// for (int i = 0; i < klingonen.getLadungsverzeichnis().size(); i++) {
		// System.out.println(klingonen.getLadungsverzeichnis().get(i));
		// klingonen.ausgabeBroadcastKommunikator();
		// }
	}
}