/**
 * 
 * @author DChojnacki
 *
 */

/**
 * ArrayList wird aus dem java.util package importiert.
 */
import java.util.ArrayList;

public class Raumschiff {
	/**
	 * Variablen werden deklariert.
	 */
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsName;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;

	/**
	 * Vollparameterisierter Konstruktor Raumschiff
	 * 
	 * @param photonentorpedoAnzahl
	 * @param energieversorgungInProzent
	 * @param schildeInProzent
	 * @param huelleInProzent
	 * @param lebenserhaltungssystemeInProzent
	 * @param schiffsName
	 * @param androidenAnzahl
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsName, int androidenAnzahl) {
		setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
		setEnergieversorgungInProzent(energieversorgungInProzent);
		setSchildeInProzent(schildeInProzent);
		setHuelleInProzent(huelleInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setAndroidenAnzahl(androidenAnzahl);
		setSchiffsName(schiffsName);
		setBroadcastKommunikator(new ArrayList<String>());
		setLadungsverzeichnis(new ArrayList<Ladung>());
	}

	/**
	 * @return Integer
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	/**
	 * @param photonentorpedoAnzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	/**
	 * @return
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * @param energieversorgungInProzent
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	/**
	 * @return
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	/**
	 * @param schildeInProzent
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	/**
	 * @return
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * @param huelleInProzent
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	/**
	 * @return
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	/**
	 * @param lebenserhaltungssystemeInProzent
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	/**
	 * @return
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * @param androidenAnzahl
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	/**
	 * @return
	 */
	public String getSchiffsName() {
		return schiffsName;
	}

	/**
	 * @param schiffsName
	 */
	public void setSchiffsName(String schiffsName) {
		this.schiffsName = schiffsName;
	}

	/**
	 * @return
	 */
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	/**
	 * @param ladungsverzeichnis
	 */
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	/**
	 * @return
	 */
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	/**
	 * @param broadcastKommunikator
	 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	/**
	 * F�gt der ArrayList<String> broadcastKommunikator einen neuen Broadcast hinzu.
	 * 
	 * @param neuBroadcast
	 */
	public void addBroadcastKommunikator(String neuBroadcast) {
		this.broadcastKommunikator.add(neuBroadcast);
	}

	/**
	 * Gibt den letzten Array des ArrayList<String> broadcastKommunikator in der
	 * Konsole aus.
	 * 
	 */
	public void ausgabeBroadcastKommunikator() {
		System.out.println(this.broadcastKommunikator.get(this.broadcastKommunikator.size() - 1));
	}

	/**
	 * Gibt die gesamte ArrayList<String> broadcastKommunikator in der Konsole aus.
	 */
	public void ausgabeLog() {
		System.out.println("[Log]" + this.broadcastKommunikator);
	}

	/**
	 * F�gt der ArrayList<Ladung> ladungsverzeichnis eine neue Ladung hinzu.
	 * 
	 * @param neueLadung
	 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * Schleife.: F�r jede Ladung im ladungsverzeichnis wird eine Ladung, mit der
	 * Menge und der Bezeichnung in die Konsole ausgegeben.
	 */
	public void ausgabeLadungsverzeichnis() {
		for (Ladung ladung : this.ladungsverzeichnis) {
			System.out.println(this.getSchiffsName() + " (Ladung): " + ladung.getMenge() + " " + ladung.getBezeichnung());
		}
	}

	/**
	 * Der Methode addBroadcastKommunikator wird ein String �bergeben. Die Methode
	 * ausgabeBroadcastKommunikator wird aufgerufen. Die Schilde des Raumschiffes
	 * werden um 50 gesenkt. Voraussetzung.: Falls die Schilde des Raumschiffes 1
	 * unterschreiten, (Effekt.:) wird die Huelle um 50 reduziert und die
	 * Energieversorgung um 50 reduziert. Voraussetzung.: Falls die Huelle 1
	 * unterschreitet, (Effekt.:) werden die Lebenserhaltungssysteme auf 0 gesetzt,
	 * der Methode addBroadcastKommunikator wird ein String �bergeben und die
	 * Methode ausgabeBroadcastKommunikator wird aufgerufen.
	 * 
	 * @param raumschiff, welches getroffen wurde.
	 */
	public void treffer(Raumschiff raumschiff) {
		addBroadcastKommunikator(raumschiff.getSchiffsName() + " wurde getroffen!");
		this.ausgabeBroadcastKommunikator();
		raumschiff.setSchildeInProzent(raumschiff.getSchildeInProzent() - 50);
		if (raumschiff.getSchildeInProzent() < 1) {
			raumschiff.setHuelleInProzent(raumschiff.getHuelleInProzent() - 50);
			raumschiff.setEnergieversorgungInProzent(raumschiff.getEnergieversorgungInProzent() - 50);
		}
		if (raumschiff.getHuelleInProzent() < 1) {
			raumschiff.setLebenserhaltungssystemeInProzent(0);
			this.addBroadcastKommunikator("Die Lebenserhaltungssysteme wurden vernichtet!");
			this.ausgabeBroadcastKommunikator();
		}
	}

	/**
	 * Voraussetzung.: Falls die Anzahl der Photonentorpedos 1 unterschreitet,
	 * (Effekt.:) wird dem BroadcastKommunikator ein String hinzugef�gt und die
	 * Methode ausgabeBroadcastKommunikator aufgerufen. Falls dies nicht der Fall
	 * ist, (Effekt.:) wird die Anzahl der Photonentorpedos um 1 verringert, dem
	 * BroadcastKommunikator ein String hinzugef�gt und die Methode treffer welcher
	 * den Parameter raumschiff bekommt, aufgerufen.
	 * 
	 * @param raumschiff, welches abschie�t.
	 */
	public void torpedoAbschiessen(Raumschiff raumschiff) {
		if (raumschiff.getPhotonentorpedoAnzahl() < 1) {
			addBroadcastKommunikator("-=*Click*=-");
			this.ausgabeBroadcastKommunikator();
		} else {
			raumschiff.setPhotonentorpedoAnzahl(raumschiff.getPhotonentorpedoAnzahl() - 1);
			addBroadcastKommunikator("Photonentorpedo abgeschossen");
			treffer(raumschiff);
		}
	}

	/**
	 * Voraussetzung.: Falls die Energieversorgung 50 unterschreitet, (Effekt.:)wird
	 * die Methode addBroadcastKommunikator aufgerufen und ein String �bergeben.
	 * Falls dies nicht der Fall ist, (Effekt.:) wird die Energieversorgung um 50
	 * verringert, die Methode addBroadcastKommunikator aufgerufen ein String
	 * hinzugef�gt und die Methode treffer welcher den Parameter raumschiff bekommt,
	 * aufgerufen.
	 * @param raumschiff, welches abschie�t.
	 */
	public void phaserkanoneAbschiessen(Raumschiff raumschiff) {
		if (raumschiff.getEnergieversorgungInProzent() < 50) {
			addBroadcastKommunikator("-=*Click*=-");
			this.ausgabeBroadcastKommunikator();
		} else {
			raumschiff.setEnergieversorgungInProzent(raumschiff.getEnergieversorgungInProzent() - 50);
			addBroadcastKommunikator("Phaserkanone abgeschossen");
			treffer(raumschiff);
		}
	}
}